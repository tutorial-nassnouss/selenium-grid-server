import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.net.URL;

public class KhouloudTest {

    private static Wait<WebDriver> wait;
    private static DesiredCapabilities capabillities;
    private static WebDriver driver;

    @BeforeClass

    public static void setUp() throws Exception {

        capabillities = DesiredCapabilities.firefox();

        /** URL is the selenium hub URL here **/

        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabillities);
        //capabillities.setBrowserName("firefox");
        wait = new WebDriverWait(driver, 6000);
    }


    @Test
    public void yaCheba() throws Exception {
        driver.get("https://www.google.com/");
        Thread.sleep(10000); //slow down for demo purposes
        WebElement element = driver.findElement(By.name("q"));
        element.sendKeys("webdriver");
        element.submit();
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        FileUtils.copyFile(scrFile, new File("screenshot.png"));

        Thread.sleep(5000);


    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() throws Exception {
        driver.quit();
    }

    }
